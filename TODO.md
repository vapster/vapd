# TODO

* User:
  * Optional informations: ❌
  * Ignore list: ❌
  * Block list: ❌
* File Sharing:
  * Queue limits: ❌
  * Direct browse: ❌
* Chat and Instant Messaging:
  * Muzzling: ❌
  * Cloaking: ❌
  * Channels:
    * MOTD: ❌
    * Kick from channels: ❌
    * Ban list: ❌
    * Invites: ❌
    * Emotes: ❌
    * Clear: ❌
    * Levels: ❌
    * User count limits: ❌
* Server Management:
  * Echo mode when user is not logged in: ❌
  * Kill / Ban / Unban / Nuke / UnNuke user: ❌
  * Redirect / Cycle client: ❌
  * Set user data port: ❌
  * Change user password: ❌
  * Set / Reload config: ❌
  * Version: ❌
  * Connection / Listen tests: ❌
  * Server Clustering: ❌