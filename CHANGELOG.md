# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.0-alpha.3] - 2023-07-30

### Added

- Create Elite user on startup
- User: Who is / Who as
- User: Levels
- Messaging: Private Messages, Channels
- Messaging: Global/Operators Messages, Message of The Day

### Changed

- Password are now stored hashed in database

## [0.1.0-alpha.2] - 2023-07-27

### Added

- User: Hotlist feature
- User: Invalid nickname check
- User: Newer login attempt / Ghost handling
- File Sharing: Browse user's files

## [0.1.0-alpha.1] - 2023-07-21

### Added

- This changelog.
- Compilation flags to chose database backend.
- `config.toml` to Git ignored files for my sanity.
- TODO list.

### Changed

- Switched from 0.0.1 to 0.1.0-alpha.1 to meet Semantic Versioning expectations.
- Moved future features to the TODO file.