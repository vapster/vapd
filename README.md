# Vapster vapd

_vapd_ is a tiny [Napster](https://en.wikipedia.org/wiki/Napster) main server written in [V](https://vlang.io/).

It is part of the [Vapster](https://gitlab.com/vapster/) project.

## Features

* User:
  * Registration: ✓
  * Invalid nickname: ✓
  * Login: ✓
  * Newer login attempt / Ghost: ✓
  * User settings:
    * Email: ✓
    * Data port: ✓
    * Link type: ✓
  * Data port checks: ✓
  * Hotlist:
    * Add to / Remove from Hotlist: ✓
    * Hotlist signon/signoff notifications: ✓
  * Who is/Who was: ✓
  * Levels: ✓
* Chat and Instant Messaging:
  * Moderator messages: ✓
  * Global messages: ✓
  * Private messages: ✓
  * Channels:
    * List / Join / Part channels: ✓
    * Channel's users list: ✓
    * Topics: ✓
    * Public messages: ✓
* File Sharing:
  * Share file: ✓
  * Share directory: ✓
  * Unshare file: ✓
  * Unshare everything: ✓
  * Search for file: ✓ (_Basic_)
    * Results Limits: ✓ (_Basic_)
  * Downloads / Uploads / Resumed: ✓
  * Alternative (Firewalled) Downloads: ✓
  * Browse user's files: ✓
* Server Management:
  * Message Of The Day: ✓
  * Server stats: ✓
  * Permanent channels: ✓
* Misc:
  * Client version checks: ✓ (_No Auto Upgrade_)

## Compatibility

_vapd_ currently implements a subset of the Napster protocol.

The implementation of the protocol is mostly based on the
[OpenNap](https://opennap.sourceforge.net/napster.txt) document.
Behavior is cross checked with the original Napster clients.

At the moment, _vapd_ can't be linked to other servers, so:

**Users connected to one server can only share and chat with other users on the same server.**

### Clients

Here is a list of clients known to work with _vapd_.

| Client            | Client String    | Version     | Release Date | Protocol |
|-------------------|------------------|-------------|--------------|----------|
| Napster           | v2.0             | v2.0 (1318) | 1999/11/11   | Original |

## Security

Please be warned that **everything** in the Napster protocol traffic (including passwords,
presence state, notifications, discussions and so on) is sent in **clear text** over the wire.

**Do not communicate sensitive information through it.**

### Passwords

Passwords are hashed using the [bcrypt](https://en.wikipedia.org/wiki/Bcrypt) function before being stored in the user database.

They are still, at the moment, visible in **clear text** in _vapd_ log output.

**Use a throw away password that you do not use for anything else to register to this service.**

## Requirements

### Load balancer

A main server without a load balancer in front of it is pretty much useless. At least for the original Napster client. Ensure you have at least one load balancer in front of your server.

Looking for a modern Napster load balancer to run and ride the memory lane? Checkout [controld](https://gitlab.com/vapster/controld)!

### Configuring client

* If you want to use the original Napster client, modify your `hosts` file to make `server.napster.com` point to your service IP.
* Otherwise, install any Napster compatible client.

Connect. Register (or use [the default Elite user](#default-elite-user)). Have fun.

## Building

### Requirements

#### Database

You need to choose at least one database backend, install it's dependency and use the matching compilation flag.

Database compilation flags are mutually exclusive.

| Database                                  | Dependency                                           | Compilation flag |
|-------------------------------------------|------------------------------------------------------|------------------|
| [SQLite](https://www.sqlite.org/)         | [db.sqlite](https://modules.vlang.io/db.sqlite.html) | `db_sqlite`      |
| [PostgreSQL](https://www.postgresql.org/) | [db.pg](https://modules.vlang.io/db.pg.html)         | `db_postgresql`  |
| All of the above                          | All of the above                                     | `db_all`         |

### Building vapd

```shell
v -d db_sqlite .
```

## Running

### Defaults

Defaults to listening on `127.0.0.1:8888` with an in-memory database.

```shell
./vapd
Vapster vapd server vX.Y.Z listening on 127.0.0.1:8888 ...
```

### Config file

You can specify a TOML configuration file:

```toml
listen = "0.0.0.0"
port = 8888

[database]
uri = "sqlite:///vapd.db"
```

```shell
./vapd -c config.toml
Vapster vapd server vX.Y.Z listening on 0.0.0.0:8888 ...
```

### Command line parameters

Command line parameters overrides defaults or config file settings.

```shell
./controld -c config.toml -l 192.168.1.1 -p 7777
Vapster vapd server vX.Y.Z listening on 192.168.1.1:7777 ...
```

## User Database

### Default Elite user

_vapd_ will detect if users have the Elite status in the database at startup.

If no Elite user is found, it will try to create one:

```
./vapd -c config.toml
✨ It seems that it is your first run of vapd as there was no Elite user found in the database. Will try to create one...

⚠️  For security reasons this will only be shown once.

🔑 Login: Cameron
🔓 Password: ------------------

🚀 Let's start!
Vapster vapd server vX.Y.Z listening on 0.0.0.0:8888 ...
```

_vapd_ will fail and refuse to start if all default nicknames used to create Elite users are already registered. If so, you will have to set yourself a user as an Elite in the database manually.

```
✨ It seems that it is your first run of vapd as there was no Elite user found in the database. Will try to create one...
🔥 Failed: All default Elite names are already registered. Please set or register a user as an elite in the database manually.
```

### Default URI

_vapd_ defaults to an in-memory _SQLite_ database URI of `sqlite:///:memory:`.

### SQLite

_vapd_ use an in-memory _SQLite_ database by default.

If you want to persist data across server restarts you need to specify an URI with a filename, like `sqlite:///vapd.db`.

### PostgreSQL

_vapd_ can be used with a _PostgreSQL_ database.

You must override the default database URI if you want to connect to a _PostgreSQL_ database,
either by using the `-D` or `-database` command line parameter or via a config file.

