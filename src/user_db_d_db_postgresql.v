import db.pg
import orm

$if db_sqlite ? {
	$compile_error('Will not compile with "db_postgresql" and "db_sqlite".')
}

fn UserDB.new(config DBConfig) !UserDB {
	db := if config.driver == 'pg' {
		orm.Connection(pg.connect(pg.Config{
			host: config.host
			port: config.port
			user: config.username
			password: config.password
			dbname: config.database
		})!)
	} else {
		eprintln('"${config.driver}" databases are not supported.')
		exit(1)
	}

	user_db := UserDB{db}
	user_db.init()!
	return user_db
}
