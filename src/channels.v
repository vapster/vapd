struct Channel {
mut:
	name      string
	limit     int
	level     UserLevel
	permanent bool
	topic     string
	users     []string
	operators []string
}

fn (s Server) handle_channels_list(mut connection Connection) {
	for channel in s.channels {
		connection.write_command(.channels_list_entry, '${channel.name} ${channel.users.len} ${channel.topic}')
	}
	connection.write_command(.channels_list, '')
}

fn (mut s Server) handle_channel_join(mut connection Connection, payload []string) {
	channel_name := payload[0]
	channel_topic := 'Welcome to the ${channel_name} channel.'

	mut user := connection.user
	if channel_name in user.channels {
		connection.write_command(.error, 'You already joined channel ${channel_name}.')
		return
	}

	for channel in s.channels {
		if channel.name == channel_name && user.nickname !in channel.users {
			// Check if channel is full
			if channel.users.len == channel.limit {
				connection.write_command(.error, 'You can not enter channel ${channel_name}, channel is full.')
				return
			}

			// Check if user level is enough
			if user.level < channel.level.int() {
				connection.write_command(.error, 'You can not enter channel ${channel_name}, channel is restricted to the ${channel.level} level.')
				return
			}

			// Ack join
			connection.write_command(.channel_join_ack, channel_name)

			// Send channel users list
			for channel_user in channel.users {
				if mut other := s.connection_from_nickname(channel_user) {
					connection.write_command(.channel_users_entry, '${channel_name} ${other.user.nickname} ${other.user.shared_files.len} ${other.user.link_type}')
				}
			}
			connection.write_command(.channel_users_end, channel_name)

			// Send channel topic
			connection.write_command(.channel_topic, '${channel_name} ${channel.topic}')

			user.channels << channel_name

			// Notify channel's users
			s.channel_notify_presence(channel_name, mut user, true)
			return
		}
	}

	// No such channel exist yet, create a temporary one
	channel := Channel{
		name: channel_name
		limit: 200
		level: UserLevel.from_string('user') or { panic(err) }
		topic: channel_topic
		users: [user.nickname]
		operators: [user.nickname]
	}
	s.channels << channel

	// Ack join
	connection.write_command(.channel_join_ack, channel_name)
	// Send empty channel users list
	connection.write_command(.channel_users_end, channel_name)
	// Send channel topic
	connection.write_command(.channel_topic, '${channel_name} ${channel.topic}')
	user.channels << channel_name
}

fn (mut s Server) handle_channel_part(mut connection Connection, payload []string) {
	channel_name := payload[0]

	mut user := connection.user
	if channel_name !in user.channels {
		connection.write_command(.error, 'You already left channel ${channel_name}.')
		return
	}

	for channel in s.channels {
		if channel.name == channel_name && user.nickname in channel.users {
			s.channel_notify_presence(channel_name, mut user, false)
			connection.write_command(.channel_part, channel_name)
			return
		}
	}

	connection.write_command(.error, 'Channel ${channel_name} does not exists!')
}

fn (mut s Server) handle_channel_say(mut connection Connection, payload []string) {
	channel_name := payload[0]
	message := payload[1..].join(' ')
	s.channel_notify_message(channel_name, connection.user.nickname, message)
}

fn (mut s Server) handle_channel_topic(mut connection Connection, payload []string) {
	channel_name := payload[0]
	topic := payload[1..].join(' ')

	for mut channel in s.channels {
		if channel.name == channel_name {
			if channel.permanent {
				connection.write_command(.error, 'You can not change the topic of the permanent channel ${channel_name}!')
				return
			}

			if connection.user.level < UserLevel.moderator.int() {
				if connection.user.nickname !in channel.operators {
					connection.write_command(.error, 'You are not an operator on channel ${channel_name}!')
					return
				}
			}

			channel.topic = topic
			s.channel_notify_topic(channel, topic)
			return
		}
	}

	connection.write_command(.error, 'Channel ${channel_name} does not exists!')
}

fn (mut s Server) channel_notify_presence(channel_name string, mut user User, joining bool) {
	for mut channel in s.channels {
		if channel.name == channel_name {
			for channel_user in channel.users {
				if channel_user == user.nickname {
					continue
				}

				if mut other := s.connection_from_nickname(channel_user) {
					payload := '${channel.name} ${user.nickname} ${user.shared_files.len} ${user.link_type}'
					if joining {
						other.write_command(.channel_user_join, payload)
					} else {
						other.write_command(.channel_user_part, payload)
					}
				}
			}

			if joining {
				channel.users << user.nickname
			} else {
				user.channels.delete(user.channels.index(channel.name))
				channel.users.delete(channel.users.index(user.nickname))
				if user.nickname in channel.operators {
					channel.operators.delete(channel.operators.index(user.nickname))
				}
				if channel.users.len == 0 && !channel.permanent {
					// Last user left in temporary channel, destroy it
					s.channels.delete(s.channels.index(channel))
				}
			}

			return
		}
	}
}

fn (mut s Server) channel_notify_message(channel_name string, nickname string, message string) {
	for channel in s.channels {
		if channel.name == channel_name {
			for channel_user in channel.users {
				if mut connection := s.connection_from_nickname(channel_user) {
					connection.write_command(.channel_says, '${channel_name} ${nickname} ${message}')
				}
			}

			return
		}
	}
}

fn (mut s Server) channel_notify_topic(channel Channel, topic string) {
	for channel_user in channel.users {
		if mut connection := s.connection_from_nickname(channel_user) {
			connection.write_command(.channel_topic, '${channel.name} ${topic}')
		}
	}
}
