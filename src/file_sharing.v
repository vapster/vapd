struct SharedFile {
	filename  string
	checksum  string
	size      int
	bitrate   int
	frequency int
	duration  int
}

fn (s Server) handle_share_add_file(mut connection Connection, payload []string) {
	filename := payload[0]
	checksum := payload[1]
	size := payload[2].int()
	bitrate := payload[3].int()
	frequency := payload[4].int()
	duration := payload[5].int()

	// Do not add duplicate
	if connection.user.shared_files.any(it.filename == filename) {
		return
	}

	shared_file := SharedFile{filename, checksum, size, bitrate, frequency, duration}
	connection.user.shared_files << shared_file
}

fn (s Server) handle_share_add_directory(mut connection Connection, payload []string) {
	directory := payload[0]
	mut i := 1
	for i < payload.len {
		filename := payload[i]#[1..-1]
		i++
		checksum := payload[i]
		i++
		size := payload[i].int()
		i++
		bitrate := payload[i].int()
		i++
		frequency := payload[i].int()
		i++
		duration := payload[i].int()
		i++

		shared_file := SharedFile{'${directory}${filename}', checksum, size, bitrate, frequency, duration}

		// Do not add duplicate
		if connection.user.shared_files.any(it.filename == shared_file.filename) {
			continue
		}
		connection.user.shared_files << shared_file
	}
}

fn (s Server) handle_share_remove_file(mut connection Connection, payload []string) {
	filename := payload.join(' ')

	// File not shared
	if !connection.user.shared_files.any(it.filename == filename) {
		return
	}

	shared_file := connection.user.shared_files.filter(it.filename == filename).first()
	connection.user.shared_files.delete(connection.user.shared_files.index(shared_file))
}

fn (s Server) handle_share_remove_all(mut connection Connection) {
	connection.user.shared_files.clear()
}

fn (mut s Server) handle_download_request(mut connection Connection, payload []string) {
	from := payload[0]
	filename := payload[1]

	if mut other := s.connection_from_nickname(from) {
		for shared_file in other.user.shared_files {
			if shared_file.filename != filename {
				continue
			}
			other.write_command(.upload_request, '${connection.user.nickname} "${filename}" ${connection.user.link_type}')
			return
		}
	}

	// File is not shared anymore or user is not online
	connection.write_command(.download_nack, '${connection.user.nickname} "${filename}"')
}

fn (mut s Server) handle_download_alt_request(mut connection Connection, payload []string) {
	from := payload[0]
	filename := payload[1]

	if mut other := s.connection_from_nickname(from) {
		for shared_file in other.user.shared_files {
			if shared_file.filename != filename {
				continue
			}
			other.write_command(.download_alt_ack, '${connection.user.nickname} ${connection.ip} ${connection.user.data_port} "${filename}" ${shared_file.checksum} ${connection.user.link_type}')
			return
		}
	}

	// File is not shared anymore or user is not online
	connection.write_command(.download_nack, '${connection.user.nickname} "${filename}"')
}

fn (mut s Server) handle_upload_ack(mut connection Connection, payload []string) {
	from := connection.user.nickname
	from_ip := connection.ip
	from_data_port := connection.user.data_port
	from_link_type := connection.user.link_type
	to := payload[0]
	filename := payload[1]
	shared_file := connection.user.shared_files.filter(it.filename == filename).first()

	if mut other := s.connection_from_nickname(to) {
		other.write_command(.download_ack, '${from} ${from_ip} ${from_data_port} "${filename}" ${shared_file.checksum} ${from_link_type}')
	}
}

fn (s Server) handle_download_resume(mut connection Connection, payload []string) {
	checksum := payload[0]
	filesize := payload[1].int()

	for c in s.connections {
		// Filter requester
		if c.user.nickname == connection.user.nickname {
			continue
		}

		for shared_file in c.user.shared_files {
			if shared_file.checksum != checksum || shared_file.size != filesize {
				continue
			}
			connection.write_command(.download_resume_entry, '${c.user.nickname} ${c.ip} ${c.user.data_port} "${shared_file.filename}" ${shared_file.checksum} ${shared_file.size} ${c.user.link_type}')
		}
	}
	connection.write_command(.download_resume_end, '')
}

fn (s Server) handle_download_start(mut connection Connection) {
	connection.user.downloads_in_progress++
	connection.user.total_downloads++
}

fn (s Server) handle_download_end(mut connection Connection) {
	connection.user.downloads_in_progress--
}

fn (s Server) handle_upload_start(mut connection Connection) {
	connection.user.uploads_in_progress++
	connection.user.total_uploads++
}

fn (s Server) handle_upload_end(mut connection Connection) {
	connection.user.uploads_in_progress--
}

fn (s Server) handle_browse_user(mut connection Connection, payload []string) {
	nickname := payload[0]

	if other := s.connection_from_nickname(nickname) {
		for shared_file in other.user.shared_files {
			connection.write_command(.browse_user_entry, '${nickname} "${shared_file.filename}" ${shared_file.checksum} ${shared_file.size} ${shared_file.bitrate} ${shared_file.frequency} ${shared_file.duration}')
		}
	}
}
