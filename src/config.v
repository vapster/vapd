import cli
import toml
import v.vmod

const (
	default_listen_ip    = '127.0.0.1'
	default_listen_port  = 8888
	default_database_uri = 'sqlite:///:memory:'
)

struct DBConfig {
mut:
	driver   string
	username string
	password string
	host     string
	port     int
	database string
	query    string
}

struct Config {
	ip       string
	port     int
	db       DBConfig
	channels []Channel
	motd     string
}

fn parse_config(cmd cli.Command) !Config {
	mut listen_ip := ''
	mut listen_port := -1
	mut db_config := parse_database_uri(default_database_uri)
	mut channels := []Channel{}
	mut motd := get_default_motd()

	// Load configuration file
	config_file := cmd.flags.get_string('config')!
	if config_file != '' {
		config_toml := toml.parse_file(config_file) or {
			panic("Failed to parse config file: '${config_file}'")
		}
		listen_ip = config_toml.value('listen').default_to(listen_ip).string()
		listen_port = config_toml.value('port').default_to(listen_port).int()

		database_uri := config_toml.value('database.uri').default_to(default_database_uri).string()

		db_config = parse_database_uri(database_uri)
		db_config.driver = config_toml.value('database.driver').default_to(db_config.driver).string()
		db_config.username = config_toml.value('database.username').default_to(db_config.username).string()
		db_config.password = config_toml.value('database.password').default_to(db_config.password).string()
		db_config.host = config_toml.value('database.host').default_to(db_config.host).string()
		db_config.port = config_toml.value('database.port').default_to(db_config.port).int()
		db_config.database = config_toml.value('database.dbname').default_to(db_config.database).string()
		db_config.query = config_toml.value('database.query').default_to(db_config.query).string()

		channels << parse_toml_channels(config_toml)

		motd = config_toml.value('motd').default_to(motd).string().trim_space()
	}

	// Load command line parameters
	cli_listen_ip := cmd.flags.get_string('listen')!
	if !cli_listen_ip.is_blank() {
		listen_ip = cli_listen_ip
	}
	cli_listen_port := cmd.flags.get_int('port')!
	if cli_listen_port > -1 {
		if cli_listen_port == 0 {
			eprintln('Failed to parse listen port from command line')
			exit(1)
		}
		listen_port = cli_listen_port
	}
	cli_database_uri := cmd.flags.get_string('database')!
	if !cli_database_uri.is_blank() {
		db_config = parse_database_uri(cli_database_uri)
	}

	// Ensure defaults
	if listen_ip == '' {
		listen_ip = default_listen_ip
	}
	if listen_port == -1 {
		listen_port = default_listen_port
	}
	if channels.len == 0 {
		channels << Channel{
			name: 'Lobby'
			limit: 200
			level: UserLevel.from_string('user') or { panic(err) }
			permanent: true
			topic: 'General chat channel.'
		}
		channels << Channel{
			name: 'Help'
			limit: 200
			level: UserLevel.from_string('user') or { panic(err) }
			permanent: true
			topic: 'Help channel.'
		}
		channels << Channel{
			name: 'Admins'
			limit: 200
			level: UserLevel.from_string('moderator') or { panic(err) }
			permanent: true
			topic: 'Administration channel.'
		}
	}

	return Config{
		ip: listen_ip
		port: listen_port
		db: db_config
		channels: channels
		motd: motd
	}
}

fn parse_database_uri(uri string) DBConfig {
	// db:driver:[//[user[:password]@][host][:port]/][dbname][?params][#fragment]

	uri_data := uri.split('/')
	driver := uri_data[0].trim_string_left('db:').trim_string_right(':')

	mut user := ''
	mut password := ''
	mut host := ''
	mut port := -1

	mut connect_data := uri_data[2].split('@')
	if connect_data.len > 1 {
		user_pass_data := connect_data[0].split(':')
		user = user_pass_data[0]
		if user_pass_data.len > 1 {
			password = user_pass_data[1]
		}
		connect_data.drop(1)
	}

	host_port_data := connect_data[0].split(':')
	host = host_port_data[0]
	if host_port_data.len > 1 {
		port = host_port_data[1].int()
	}

	database_data := uri_data#[3..].join('/').split('?')
	database := database_data[0]

	query := if database_data.len > 1 {
		database_data[1]
	} else {
		''
	}

	return DBConfig{driver, user, password, host, port, database, query}
}

fn parse_toml_channels(doc toml.Doc) []Channel {
	mut channels := []Channel{}

	doc_channels := doc.value_opt('channels') or { return channels }

	for channel in doc_channels.array() {
		channel_name := channel.value('name').string()
		channel_limit := channel.value('limit').int()
		channel_level_str := channel.value('level').string()
		channel_level := UserLevel.from_string(channel_level_str) or {
			panic("Unknown user level for channel '${channel_name}': '${channel_level_str}'")
		}
		channel_topic := channel.value('topic').string()

		channels << Channel{
			name: channel_name
			limit: channel_limit
			level: channel_level
			permanent: true
			topic: channel_topic
		}
	}
	return channels
}

fn get_default_motd() string {
	vm := vmod.decode(@VMOD_FILE) or { panic(err) }
	return 'Welcome to Vapster ${vm.name} version v${vm.version}.

vapd is a tiny Napster main server written in V.
It is part of the Vapster project https://gitlab.com/vapster.

Thank you for using vapd!'
}
