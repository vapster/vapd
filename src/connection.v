import time
import net

struct Connection {
mut:
	socket net.TcpConn
	ip     u32
	ip_str string
	port   int
	user   &User
	time   time.Time
}

fn Connection.new(mut socket net.TcpConn) Connection {
	socket.set_read_timeout(5 * time.minute)
	socket.set_write_timeout(5 * time.minute)
	peer_ip_port := socket.peer_ip() or { panic('peer_ip') }
	peer_ip := peer_ip_port.split(':').first()
	peer_port := peer_ip_port.split(':').last().int()
	peer_ip_data := peer_ip.split('.')
	peer_ip_int := (peer_ip_data[3].u32() << 24) | (peer_ip_data[2].u32() << 16) | (peer_ip_data[1].u32() << 8) | (peer_ip_data[0].u32())
	return Connection{socket, peer_ip_int, peer_ip, peer_port, &User{}, time.now()}
}

fn (mut c Connection) close() {
	c.socket.close() or { return }
}

fn (mut c Connection) read_command() ?Command {
	return read_command(mut c.socket) or { return none }
}

fn (mut c Connection) write_command(function CommandID, payload string) {
	write_command(mut c.socket, function, payload)
}

fn (c Connection) since() time.Duration {
	return time.now() - c.time
}
