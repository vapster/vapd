import net
import regex
import time

fn (mut s Server) handle_login(mut connection Connection, payload []string) {
	if payload.len < 5 {
		connection.write_command(.login_error, 'Wrong parameters count.')
		return
	}

	nickname := payload[0]
	password := payload[1]
	data_port := payload[2].int()
	client_info := payload[3]#[1..-1]
	link_type := payload[4].int()
	// client_build_number := payload[5]

	user := s.user_db.login(nickname, password) or {
		connection.write_command(.login_error, 'Invalid password.')
		return
	}

	// We do not allow ghosts
	for mut c in s.connections {
		if c.user.nickname == nickname {
			c.write_command(.user_ghost, '')
			s.remove_connection(mut c)
			break
		}
	}

	connection.user.nickname = nickname
	connection.user.email = user.email
	connection.user.level = user.level
	connection.user.client_info = client_info
	connection.user.link_type = link_type
	connection.user.data_port = data_port

	connection.write_command(.login_ack, user.email)

	// Update Last seen time in database
	s.user_db.set_last_seen_at(nickname, time.now())

	// Send Message Of The Day
	s.handle_message_of_the_day(mut connection)

	// Notify Hotlist listeners
	s.hotlist_notify(nickname, link_type, true)
}

fn (s Server) handle_nickname_register(mut connection Connection, payload []string) {
	nickname := payload[0]
	password := payload[1]
	data_port := payload[2].int()
	client_info := payload[3]#[1..-1]
	link_type := payload[4].int()
	email := payload[5]

	level := UserLevel.from_string('user') or { panic(err) }.int()
	connection.user.nickname = nickname
	connection.user.password = password
	connection.user.email = email
	connection.user.level = level
	connection.user.registered_at = time.now()
	connection.user.last_seen_at = time.now()
	connection.user.client_info = client_info
	connection.user.link_type = link_type
	connection.user.data_port = data_port

	s.user_db.register(mut connection.user)
	connection.write_command(.login_ack, email)
}

fn (s Server) handle_nickname_check(mut connection Connection, payload []string) {
	nickname := payload[0]

	valid_nickname := r'^[a-zA-Z0-9_\[\]\{}\-@\^!\$]+$'
	re := regex.regex_opt(valid_nickname) or { panic(err) }
	if !re.matches_string(nickname) {
		connection.write_command(.nickname_invalid, '')
		return
	}

	if s.user_db.nickname_check(nickname) {
		connection.write_command(.nickname_already_registered, '')
	} else {
		connection.write_command(.nickname_not_registered, '')
	}
}

fn (s Server) handle_password_check(mut connection Connection, payload []string) {
	if payload.len != 2 {
		connection.write_command(.login_error, 'Wrong parameters count.')
		return
	}

	nickname := payload[0]
	password := payload[1]

	s.user_db.login(nickname, password) or {
		connection.write_command(.login_error, 'Invalid password.')
		return
	}

	connection.write_command(.password_verified, '')
}

fn (s Server) handle_user_data_port(mut connection Connection, payload []string) {
	data_port := payload[0].int()
	connection.user.data_port = data_port
}

fn (mut s Server) handle_user_data_port_error(mut connection Connection, payload []string) {
	nickname := payload[0]
	if mut other := s.connection_from_nickname(nickname) {
		other.write_command(.user_data_port_error, '${connection.user.nickname}')
	}
}

fn (s Server) handle_user_check_data_port(mut connection Connection, payload []string) {
	data_port := payload[0].int()
	net.dial_tcp('${connection.ip_str}:${data_port}') or {}
}

fn (s Server) handle_user_link_type(mut connection Connection, payload []string) {
	link_type := payload[0].int()
	connection.user.link_type = link_type
}

fn (s Server) handle_link_type_request(mut connection Connection, payload []string) {
	nickname := payload[0]
	if mut other := s.connection_from_nickname(nickname) {
		connection.write_command(.link_type, '${other.user.link_type}')
	}
}

fn (s Server) handle_user_password(mut connection Connection, payload []string) {
	password := payload[0]
	s.user_db.set_password(connection.user.nickname, password)
}

fn (s Server) handle_user_email(mut connection Connection, payload []string) {
	email := payload[0]
	s.user_db.set_email(connection.user.nickname, email)
}

fn (s Server) handle_user_whois_request(mut connection Connection, payload []string) {
	nickname := payload[0]

	// Check if user is online
	if other := s.connection_from_nickname(nickname) {
		level := UserLevel.from_int(other.user.level)
		since := int(other.since().seconds())
		channels := other.user.channels.join(' ') + ' '
		shared_files := other.user.shared_files.len
		downloads := other.user.downloads_in_progress
		uploads := other.user.uploads_in_progress
		link_type := other.user.link_type
		client_info := other.user.client_info
		mut whois_info := '${nickname} "${level}" ${since} "${channels}" "Active" ${shared_files} ${downloads} ${uploads} ${link_type} "${client_info}"'

		if connection.user.level > UserLevel.moderator.int() {
			total_downloads := other.user.total_downloads
			total_uploads := other.user.total_uploads
			ip := other.ip_str
			connecting_port := other.port
			data_port := other.user.data_port
			email := other.user.email
			whois_info += ' ${total_downloads} ${total_uploads} ${ip} ${connecting_port} ${data_port} ${email}'
		}

		connection.write_command(.user_whois, whois_info)
		return
	}

	// User is not online anymore, check latest information from database
	if user := s.user_db.get(nickname) {
		level := UserLevel.from_int(user.level)
		last_seen_at := user.last_seen_at.unix_time()
		whowas_info := '${user.nickname} "${level}" ${last_seen_at}'
		connection.write_command(.user_whowas, whowas_info)
		return
	}

	// Failed to retrieve user or user does not exists
	connection.write_command(.error, 'Failed to retrieve whois information for user ${nickname}.')
}

fn (s Server) handle_private_message(mut connection Connection, payload []string) {
	from := connection.user.nickname
	to := payload[0]
	message := payload[1..].join(' ')

	if mut other := s.connection_from_nickname(to) {
		other.write_command(.message_private, '${from} ${message}')
	}
}
