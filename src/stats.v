import arrays

const (
	gigabyte = (1024 * 1024 * 1024)
)

fn (s Server) handle_stats(mut connection Connection) {
	online_users := s.connections.len
	mut library_file_count := 0
	mut library_size := 0
	for c in s.connections {
		library_file_count += c.user.shared_files.len
		library_size += arrays.fold[SharedFile, int](c.user.shared_files, 0, fn (r int, file SharedFile) int {
			return r + file.size
		})
	}
	library_size_gb := library_size / gigabyte
	connection.write_command(.server_stats, '${online_users} ${library_file_count} ${library_size_gb}')
}
