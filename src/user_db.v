import orm
import rand
import time

const (
	elite_names      = ['Cameron', 'Gordon', 'Joe', 'Donna', 'John']
	password_max_len = 18 // Original Napster client limitation
)

struct UserDB {
	db orm.Connection
}

fn (u UserDB) init() ! {
	sql u.db {
		create table User
	}!

	u.create_elite()!
}

fn (u UserDB) create_elite() ! {
	// Check for Elite user
	results := sql u.db {
		select from User where level == UserLevel.elite.int()
	} or { panic('UserDB: ${err}') }

	if results.len == 0 {
		// No Elite user found, register a new one
		println('✨ It seems that it is your first run of vapd as there was no Elite user found in the database. Will try to create one...')

		mut elites := elite_names.clone()
		mut elite_choosen_one := ''
		rand.shuffle(mut elites)!
		for elite in elites {
			u.get(elite) or {
				elite_choosen_one = elite
				break
			}
		}

		if elite_choosen_one.is_blank() {
			eprint('🔥 Failed: All default Elite names are already registered. ')
			eprintln('Please set or register a user as an elite in the database manually.')
			exit(1)
		}

		elite_password := rand.string(password_max_len)
		mut elite := User{
			nickname: elite_choosen_one
			level: UserLevel.elite.int()
			registered_at: time.now()
			email: '${elite_choosen_one}@example.com'
			password: elite_password
		}
		u.register(mut elite)

		println('')
		println('⚠️  For security reasons this will only be shown once.')
		println('')
		println('🔑 Login: ${elite_choosen_one}')
		println('🔓 Password: ${elite_password}')
		println('')
		println("🚀 Let's start!")
	}
}

fn (u UserDB) nickname_check(nickname string) bool {
	results := sql u.db {
		select from User where nickname == nickname
	} or { panic('UserDB: ${err}') }
	return results.len != 0
}

fn (u UserDB) login(nickname string, password string) ?User {
	if user := u.get(nickname) {
		if check_password(password, user.password) {
			return user
		}
	}
	return none
}

fn (u UserDB) register(mut user User) {
	user.password = hash_password(user.password)

	sql u.db {
		insert user into User
	} or { panic('UserDB: ${err}') }
}

fn (u UserDB) get(nickname string) ?User {
	results := sql u.db {
		select from User where nickname == nickname
	} or { panic('UserDB: ${err}') }
	if results.len == 0 {
		return none
	}
	return results.first()
}

fn (u UserDB) set_password(nickname string, password string) {
	hashed_password := hash_password(password)

	sql u.db {
		update User set password = hashed_password where nickname == nickname
	} or { panic('UserDB: ${err}') }
}

fn (u UserDB) set_email(nickname string, email string) {
	sql u.db {
		update User set email = email where nickname == nickname
	} or { panic('UserDB: ${err}') }
}

fn (u UserDB) set_last_seen_at(nickname string, last_seen_at time.Time) {
	sql u.db {
		update User set last_seen_at = last_seen_at where nickname == nickname
	} or { panic('UserDB: ${err}') }
}
