import crypto.bcrypt

fn hash_password(password string) string {
	hashed_password := bcrypt.generate_from_password(password.bytes(), bcrypt.default_cost) or {
		panic(err)
	}
	return hashed_password
}

fn check_password(password string, hash string) bool {
	bcrypt.compare_hash_and_password(password.bytes(), hash.bytes()) or { return false }
	return true
}
