fn (s Server) handle_client_version_check(mut connection Connection, payload []string) {
	// Allow all clients, never propose upgrade
	connection.write_command(.client_version_check, '')
}
