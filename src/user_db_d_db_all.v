import db.pg
import db.sqlite
import orm

$if db_sqlite ? {
	$compile_error('Will not compile with "db_all" and "db_sqlite".')
}
$if db_postgresql ? {
	$compile_error('Will not compile with "db_all" and "db_postgresql".')
}

fn UserDB.new(config DBConfig) !UserDB {
	db := match config.driver {
		'sqlite' {
			orm.Connection(sqlite.connect(config.database)!)
		}
		'pg' {
			orm.Connection(pg.connect(pg.Config{
				host: config.host
				port: config.port
				user: config.username
				password: config.password
				dbname: config.database
			})!)
		}
		else {
			eprintln('"${config.driver}" databases are not supported.')
			exit(1)
		}
	}

	user_db := UserDB{db}
	user_db.init()!
	return user_db
}
