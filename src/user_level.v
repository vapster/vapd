enum UserLevel {
	user = 0
	moderator = 2
	admin = 3
	elite = 4
}

fn (u UserLevel) int() int {
	return int(u)
}

fn (u UserLevel) str() string {
	match u {
		.user { return 'User' }
		.moderator { return 'Moderator' }
		.admin { return 'Admin' }
		.elite { return 'Elite' }
	}
}

fn UserLevel.from_string(level string) !UserLevel {
	match level.to_lower() {
		'user' {
			return .user
		}
		'moderator' {
			return .moderator
		}
		'admin' {
			return .admin
		}
		'elite' {
			return .elite
		}
		else {
			return error('Unknown UserLevel: ${level}')
		}
	}
}

fn UserLevel.from_int(level int) UserLevel {
	return unsafe { UserLevel(level) }
}
