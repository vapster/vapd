import net
import v.vmod

struct Server {
	listen_ip   string
	listen_port int
	user_db     UserDB
	motd        string
mut:
	connections []Connection
	hotlist     map[string][]string
	channels    []Channel
}

fn Server.new(config Config) Server {
	return Server{
		listen_ip: config.ip
		listen_port: config.port
		user_db: UserDB.new(config.db) or { panic('Could not connect to database: ${err}') }
		connections: []
		hotlist: map[string][]string{}
		channels: config.channels
		motd: config.motd
	}
}

fn (mut s Server) listen() {
	mut listener := net.listen_tcp(.ip, '${s.listen_ip}:${s.listen_port}') or {
		panic('listen_tcp')
	}
	listen_address := listener.addr() or { panic('addr') }

	vm := vmod.decode(@VMOD_FILE) or { panic(err) }
	println('Vapster ${vm.name} server v${vm.version} listening on ${listen_address} ...')
	for {
		mut socket := listener.accept() or { panic('accept') }
		spawn s.handle_connection(mut socket)
	}
}

fn (mut s Server) add_connection(mut socket net.TcpConn) &Connection {
	connection := Connection.new(mut socket)
	s.connections << &connection
	return &connection
}

fn (mut s Server) remove_connection(mut connection Connection) {
	if connection in s.connections {
		if !connection.user.nickname.is_blank() {
			s.logout_user(mut connection.user)
		}
		s.connections.delete(s.connections.index(connection))
		connection.close()
	}
}

fn (s Server) connection_from_nickname(nickname string) ?Connection {
	connections := s.connections.filter(it.user.nickname == nickname)
	if connections.len > 0 {
		return connections.first()
	}
	return none
}

fn (mut s Server) handle_connection(mut socket net.TcpConn) {
	mut connection := s.add_connection(mut socket)
	defer {
		s.remove_connection(mut connection)
	}
	connection_address := socket.peer_addr() or { return }
	eprintln('<! ${connection_address}')

	for {
		command := connection.read_command() or {
			eprintln('<X ${connection_address}')
			break
		}
		match command.function {
			// Login
			.login {
				s.handle_login(mut connection, command.payload)
			}
			.nickname_register {
				s.handle_nickname_register(mut connection, command.payload)
			}
			.nickname_check {
				s.handle_nickname_check(mut connection, command.payload)
			}
			.password_check {
				s.handle_password_check(mut connection, command.payload)
			}
			// User
			.user_check_data_port {
				s.handle_user_check_data_port(mut connection, command.payload)
			}
			.user_data_port_error {
				s.handle_user_data_port_error(mut connection, command.payload)
			}
			.user_link_type {
				s.handle_user_link_type(mut connection, command.payload)
			}
			.user_password {
				s.handle_user_password(mut connection, command.payload)
			}
			.user_email {
				s.handle_user_email(mut connection, command.payload)
			}
			.user_data_port {
				s.handle_user_data_port(mut connection, command.payload)
			}
			.link_type_request {
				s.handle_link_type_request(mut connection, command.payload)
			}
			.user_whois_request {
				s.handle_user_whois_request(mut connection, command.payload)
			}
			// File Sharing
			.share_add_file {
				s.handle_share_add_file(mut connection, command.payload)
			}
			.share_add_directory {
				s.handle_share_add_directory(mut connection, command.payload)
			}
			.share_remove_file {
				s.handle_share_remove_file(mut connection, command.payload)
			}
			.share_remove_all {
				s.handle_share_remove_all(mut connection)
			}
			.search {
				s.handle_search(mut connection, command.payload)
			}
			.download_resume {
				s.handle_download_resume(mut connection, command.payload)
			}
			.download_request {
				s.handle_download_request(mut connection, command.payload)
			}
			.download_alt_request {
				s.handle_download_alt_request(mut connection, command.payload)
			}
			.download_start {
				s.handle_download_start(mut connection)
			}
			.download_end {
				s.handle_download_end(mut connection)
			}
			.upload_start {
				s.handle_upload_start(mut connection)
			}
			.upload_end {
				s.handle_upload_end(mut connection)
			}
			.upload_ack {
				s.handle_upload_ack(mut connection, command.payload)
			}
			.browse_user {
				s.handle_browse_user(mut connection, command.payload)
			}
			// Hotlist
			.hotlist_add_user, .hotlist_add_user_at_login {
				s.handle_hotlist_add_user(mut connection, command.payload)
			}
			.hotlist_remove_user {
				s.handle_hotlist_remove_user(mut connection, command.payload)
			}
			// Chat & Instant Messaging
			.message_private {
				s.handle_private_message(mut connection, command.payload)
			}
			.channels_list {
				s.handle_channels_list(mut connection)
			}
			.channel_join {
				s.handle_channel_join(mut connection, command.payload)
			}
			.channel_part {
				s.handle_channel_part(mut connection, command.payload)
			}
			.channel_say {
				s.handle_channel_say(mut connection, command.payload)
			}
			.channel_topic {
				s.handle_channel_topic(mut connection, command.payload)
			}
			// Administration
			.message_of_the_day {
				s.handle_message_of_the_day(mut connection)
			}
			.message_announce {
				s.handle_announce(mut connection, command.payload)
			}
			.message_operators {
				s.handle_opsay(mut connection, command.payload)
			}
			// Stats
			.server_stats {
				s.handle_stats(mut connection)
			}
			// Clients
			.client_version_check {
				s.handle_client_version_check(mut connection, command.payload)
			}
			else {
				// Unimplemented
			}
		}
	}
}
