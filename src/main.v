import cli
import os
import v.vmod

$if !db_all ? {
	$if db_sqlite ? {
	} $else $if db_postgresql ? {
	} $else {
		$compile_error('Will not compile without "db_all", "db_sqlite" or "db_postgresql".')
	}
}

fn main() {
	vm := vmod.decode(@VMOD_FILE) or { panic(err) }
	mut app := cli.Command{
		name: '${vm.name}'
		description: '${vm.description}'
		version: '${vm.version}'
		execute: run
		flags: [
			cli.Flag{
				flag: .string
				name: 'listen'
				abbrev: 'l'
				description: 'Listens for connections on given IP.'
				default_value: ['']
			},
			cli.Flag{
				flag: .int
				name: 'port'
				abbrev: 'p'
				description: 'Listens for connections on given port.'
				default_value: ['-1']
			},
			cli.Flag{
				flag: .string
				name: 'database'
				abbrev: 'D'
				description: 'Connect to given backend database URI.'
				default_value: ['']
			},
			cli.Flag{
				flag: .string
				name: 'config'
				abbrev: 'c'
				description: 'Load configuration from given TOML file.'
			},
		]
	}
	app.setup()
	app.parse(os.args)
}

fn run(cmd cli.Command) ! {
	mut server := Server.new(parse_config(cmd)!)
	server.listen()
}
