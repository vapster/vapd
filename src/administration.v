fn (s Server) handle_message_of_the_day(mut connection Connection) {
	for line in s.motd.split('\n') {
		connection.write_command(.message_of_the_day, line)
	}
}

fn (mut s Server) handle_announce(mut connection Connection, payload []string) {
	from := connection.user.nickname
	message := payload.join(' ')
	if connection.user.level > UserLevel.moderator.int() {
		for mut c in s.connections {
			c.write_command(.message_announce, '${from} ${message}')
		}
	} else {
		connection.write_command(.error, 'Permission denied.')
	}
}

fn (mut s Server) handle_opsay(mut connection Connection, payload []string) {
	from := connection.user.nickname
	message := payload.join(' ')
	if connection.user.level > UserLevel.moderator.int() {
		for mut c in s.connections {
			if c.user.level >= UserLevel.moderator.int() {
				c.write_command(.message_operators, '${from} ${message}')
			}
		}
	} else {
		connection.write_command(.error, 'Permission denied.')
	}
}
