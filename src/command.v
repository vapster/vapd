import encoding.binary
import net

enum CommandID as u16 {
	login_error = 0
	login = 2
	login_ack = 3
	client_version_check = 4
	client_upgrade = 5
	nickname_register = 6
	nickname_check = 7
	nickname_not_registered = 8
	nickname_already_registered = 9
	nickname_invalid = 10
	password_check = 11
	password_verified = 12
	share_add_file = 100
	share_remove_file = 102
	share_remove_all = 110
	search = 200
	search_entry = 201
	search_end = 202
	download_request = 203
	download_ack = 204
	message_private = 205
	download_nack = 206
	hotlist_add_user = 207
	hotlist_add_user_at_login = 208
	hotlist_user_sign_on = 209
	hotlist_user_sign_off = 210
	browse_user = 211
	browse_user_entry = 212
	browse_user_end = 213
	server_stats = 214
	download_resume = 215
	download_resume_entry = 216
	download_resume_end = 217
	download_start = 218
	download_end = 219
	upload_start = 220
	upload_end = 221
	user_check_data_port = 300
	hotlist_ack = 301
	hotlist_nack = 302
	hotlist_remove_user = 303
	channel_join = 400
	channel_part = 401
	channel_say = 402
	channel_says = 403
	error = 404
	channel_join_ack = 405
	channel_user_join = 406
	channel_user_part = 407
	channel_users_entry = 408
	channel_users_end = 409
	channel_topic = 410
	download_alt_request = 500
	download_alt_ack = 501
	link_type_request = 600
	link_type = 601
	user_whois_request = 603
	user_whois = 604
	user_whowas = 605
	upload_request = 607
	upload_ack = 608
	upload_nack = 609
	channels_list = 617
	channels_list_entry = 618
	message_of_the_day = 621
	user_data_port_error = 626
	message_operators = 627
	message_announce = 628
	user_link_type = 700
	user_password = 701
	user_email = 702
	user_data_port = 703
	user_ghost = 748
	share_add_directory = 870
}

struct Command {
	length   u16
	function CommandID
	payload  []string
}

fn read_command(mut socket net.TcpConn) ?Command {
	mut buf := []u8{len: 4096}
	socket.read(mut buf) or { return none }
	length := binary.little_endian_u16(buf[0..1])
	function := binary.little_endian_u16(buf[2..3])
	mut payload := []string{}

	if length > 0 {
		mut quoted_token := []string{}
		mut in_quote := false
		for token in buf[4..4 + length].bytestr().split(' ') {
			if in_quote {
				quoted_token << token
				if token.ends_with('"') {
					in_quote = false
					payload << quoted_token.join(' ')#[1..-1]
				}
			} else {
				if token.starts_with('"') && !token.ends_with('"') {
					in_quote = true
					quoted_token << token
				} else {
					payload << token
				}
			}
		}
	}

	command_id := unsafe { CommandID(function) }
	eprintln('< [${command_id:25}] ${length}:${function}:${payload}')
	return Command{length, command_id, payload}
}

fn write_command(mut socket net.TcpConn, function CommandID, payload string) {
	length := payload.bytes().len
	mut buffer := []u8{len: 4}
	binary.little_endian_put_u16(mut buffer, u16(length))
	binary.little_endian_put_u16_at(mut buffer, u16(function), 2)
	buffer.insert(4, payload.bytes())
	eprintln('> [${function:25}] ${length}:${u16(function)}:${payload}')
	socket.write(buffer) or { panic(err) }
}
