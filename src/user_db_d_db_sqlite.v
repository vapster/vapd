import db.sqlite
import orm

$if db_postgresql ? {
	$compile_error('Will not compile with "db_sqlite" and "db_postgresql".')
}

fn UserDB.new(config DBConfig) !UserDB {
	db := if config.driver == 'sqlite' {
		orm.Connection(sqlite.connect(config.database)!)
	} else {
		eprintln('"${config.driver}" databases are not supported.')
		exit(1)
	}

	user_db := UserDB{db}
	user_db.init()!
	return user_db
}
