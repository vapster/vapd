struct SearchResult {
	file      SharedFile
	nickname  string
	ip        u32
	link_type int
	weight    int
}

fn (s Server) handle_search(mut connection Connection, payload []string) {
	mut max_results := 100
	mut i := 0
	mut search := ''
	for i < payload.len {
		token := payload[i]
		i++
		if token == 'MAX_RESULTS' {
			max_results = payload[i].int()
			i++
		}
		if token == 'FILENAME' {
			i++ // CONTAINS
			search = payload[i]#[1..-1]
			i++
		}
	}

	mut search_results := []SearchResult{}
	for c in s.connections {
		// Filter requester
		if c.user.nickname == connection.user.nickname {
			continue
		}

		search_results << c.user.shared_files.filter(it.filename.contains(search)).map(fn [c] (s SharedFile) SearchResult {
			return SearchResult{
				file: s
				nickname: c.user.nickname
				ip: c.ip
				link_type: c.user.link_type
				weight: 5
			}
		})
	}
	mut results_sent := 0
	for result in search_results {
		connection.write_command(.search_entry, '"${result.file.filename}" ${result.file.checksum} ${result.file.size} ${result.file.bitrate} ${result.file.frequency} ${result.file.duration} ${result.nickname} ${result.ip} ${result.link_type} ${result.weight}')
		results_sent++
		if results_sent > max_results {
			break
		}
	}
	connection.write_command(.search_end, '')
}
