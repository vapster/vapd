import time

struct User {
mut:
	nickname              string       [primary]
	password              string
	email                 string
	level                 int
	registered_at         time.Time    [sql_type: 'TIMESTAMP']
	last_seen_at          time.Time    [sql_type: 'TIMESTAMP']
	client_info           string       [skip]
	link_type             int          [skip]
	data_port             int          [skip]
	shared_files          []SharedFile [skip]
	downloads_in_progress int          [skip]
	uploads_in_progress   int          [skip]
	total_downloads       int          [skip]
	total_uploads         int          [skip]
	channels              []string     [skip]
}

fn (mut s Server) logout_user(mut user User) {
	// Remove user from joined channels
	for channel in user.channels {
		s.channel_notify_presence(channel, mut user, false)
	}

	// Notify Hotlist listeners
	s.hotlist_notify(user.nickname, 0, false)

	// Update Last seen time in database
	s.user_db.set_last_seen_at(user.nickname, time.now())
}
