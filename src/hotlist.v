fn (mut s Server) handle_hotlist_add_user(mut connection Connection, payload []string) {
	nickname := payload[0]

	// TODO Should send nack for unregistered nickname

	if mut hotlist := s.hotlist[nickname] {
		if connection.user.nickname !in hotlist {
			hotlist << connection.user.nickname
		}
	} else {
		s.hotlist[nickname] = [connection.user.nickname]
	}

	connection.write_command(.hotlist_ack, nickname)

	// Check if hotlisted user is currently online
	if mut other := s.connection_from_nickname(nickname) {
		connection.write_command(.hotlist_user_sign_on, '${nickname} ${other.user.link_type}')
	}
}

fn (mut s Server) handle_hotlist_remove_user(mut connection Connection, payload []string) {
	nickname := payload[0]

	if mut hotlist := s.hotlist[nickname] {
		if connection.user.nickname in hotlist {
			hotlist.delete(hotlist.index(connection.user.nickname))
			s.hotlist[nickname] = hotlist
		}
	}
}

fn (mut s Server) hotlist_notify(nickname string, link_type int, sign_on bool) {
	if hotlist := s.hotlist[nickname] {
		for mut c in s.connections {
			if c.user.nickname in hotlist {
				if sign_on {
					c.write_command(.hotlist_user_sign_on, '${nickname} ${link_type}')
				} else {
					c.write_command(.hotlist_user_sign_off, nickname)
				}
			}
		}
	}
}
